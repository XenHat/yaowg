#!/bin/bash

subreddit=animewallpaperssfw
subreddit_sort_mode=latest
subreddit_time_scale=week
subreddit_result_limit=100
cache_time=1440
reply_cache_file="$HOME/.cache/reddit.json"

if [[ ! -d "$(dirname $reply_cache_file)" ]]; then
	mkdir -p "$(dirname $reply_cache_file)"
fi

function download_list() {
	echo "Downloading new posts list..."
	url=("https://www.reddit.com/r/")
	url+=("${subreddit}/new.json?sort=${subreddit_sort_mode}")
	url+=("&t=${subreddit_time_scale}")
	url+=("&limit=${subreddit_result_limit}")
	concat_url="${url[*]}"
	trimmed_url=$(echo "${concat_url}" | tr -d ' ')
	echo "DEBUG: Fetching from ${trimmed_url}"
	IFS=
	reply="$(curl --fail --silent ${trimmed_url} 2>&1)"
	if [[ -z "$reply" ]]; then
		echo "Failure, keeping old file"
	else
		echo "Success"
		echo "$reply" > "$reply_cache_file"
	fi
}
if  [[ "$(cat $reply_cache_file)" == '{"message": "Too Many Requests", "error": 429}' ]]; then
	echo "last request was an error from old version of this script; try again"
	download_list
elif [[ -f "$reply_cache_file" ]] && test "$(find "$reply_cache_file" -mmin -"$cache_time")"; then
	echo "using cached posts list"
else
	download_list
fi

