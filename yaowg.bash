#!/bin/bash
DIR="$(cd "$(dirname "$0")" && pwd)"
set -eo pipefail
bash "${DIR}"/fetch_wallpapers_list.bash
# work around https://bugs.kde.org/show_bug.cgi?id=387663
rm ~/.cache/wallpaper -f
python3 "${DIR}"/download_wallpaper.py
#if [[ $- == *i* ]]; then
command -v timg >/dev/null 2>&1 && timg ~/.cache/wallpaper
#fi
if [[ "$1" != "--no-apply" ]]; then
  if [[ "$XDG_CURRENT_DESKTOP" == "KDE" ]]; then
    python3 "${DIR}"/set_wallpaper.py
  elif [[ "$XDG_CURRENT_DESKTOP" == "Hyprland" ]]; then
	if pgrep hyprpaper; then kill -USR1 $(pidof hyprpaper); fi
	command -v hyprpaper && hyprpaper & disown
  elif [[ -n "$SWAYSOCK" ]]; then
    swaybg -m fill -i ~/.cache/wallpaper &
    disown
  elif [[ -z "$WAYLAND_DISPLAY" ]]; then
    feh --bg-scale "$HOME/.cache/wallpaper"
  fi
fi
