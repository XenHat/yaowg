#!/bin/python3
from pathlib import Path
import dbus

home_folder=Path.home()
wallpaper_file_path = home_folder / ".cache/wallpaper"

if __name__ == '__main__':
    bus = dbus.SessionBus()
    plasma = dbus.Interface(bus.get_object('org.kde.plasmashell', '/PlasmaShell'), dbus_interface='org.kde.PlasmaShell')
    plugin = 'org.kde.image'
    jscript = """
        var allDesktops = desktops();
        print (allDesktops);
        for (i=0;i<allDesktops.length;i++) {
            d = allDesktops[i];
            d.wallpaperPlugin = "org.kde.image";
            d.currentConfigGroup = Array("Wallpaper", "org.kde.image", "General");
            d.writeConfig("Image", "file://%s")
        }
        """
    # https://bugs.kde.org/show_bug.cgi?id=387663
    plasma.evaluateScript(jscript % ("/dev/null"))
    plasma.evaluateScript(jscript % (wallpaper_file_path))



