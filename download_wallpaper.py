#!/bin/python3
from pathlib import Path
import json
import requests
import shutil
import random

cache_path = Path.home() / ".cache"


def build_wallpaper_list():
    the_list = []
    f = open(cache_path / "reddit.json")
    json_data = json.load(f)
    f.close()
    for item in json_data["data"]["children"]:
        if item["kind"] != "t3":
            break
        data = item["data"]
        platform = data["link_flair_text"]
        if platform == "Desktop":
            url = ""
            if "is_video" not in data:
                continue
            if "url" not in data:
                continue
            if data["is_video"]:
                continue
            if "url_overridden_by_dest" in data:
                url = data["url_overridden_by_dest"]
            else:
                url = data["url"]
            # idiots don't mark video wallpapers as video...
            if "i.redd.it" not in url:
                continue
            # print(data["title"], " ", url)
            the_list += [[data["title"], url]]
    return the_list


if __name__ == "__main__":
    desktop_wallpapers = build_wallpaper_list()
    random_index = random.randint(0, len(desktop_wallpapers) - 1)
    random_wallpaper = desktop_wallpapers[random_index]
    print(random_wallpaper[0])
    # Download the selected wallpaper
    res = requests.get(random_wallpaper[1], stream=True)
    if res.status_code == 200:
        with open(cache_path / "wallpaper", "wb") as f:
            shutil.copyfileobj(res.raw, f)
    else:
        print("Image Couldn't be retrieved")
